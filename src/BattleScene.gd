extends Node

const fighter_status = preload("res://src/common.gd")

var CurrentStatus = Common.fighter_status.Neutral

onready var PlayerNode = get_node("/root/Node2D/Character")

# Called when the node enters the scene tree for the first time.
func _ready():
	PlayerNode.CurrentStatus = Common.fighter_status.Winding_Up
	print(PlayerNode.CurrentStatus)
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func ProcessAttack() -> int:
	var TotalDamage = 0
	match CurrentStatus:
		fighter_status.Neutral:
			TotalDamage = 20
		fighter_status.Attacking:
			TotalDamage = 25
		fighter_status.Winding_Up:
			TotalDamage = 40
		fighter_status.Guarding:
			TotalDamage = 10
		fighter_status.Recoiling:
			TotalDamage = 0
	return TotalDamage

func UpdateStatus(Status):
	get_node("StatusLabel").text = "Status: %s" % Status

func _on_AttackButton_pressed():
	UpdateStatus("Attacking")
	var CurrentHealth = get_node("EnemyHealthBarGroup/HealthBar").rect_size.x
	var TotalDamage = ProcessAttack()
	get_node("EnemyHealthBarGroup/HealthBar").rect_size.x -= TotalDamage if CurrentHealth > TotalDamage else CurrentHealth
	CurrentStatus = fighter_status.Attacking

func _on_GuardButton_pressed():
	CurrentStatus = fighter_status.Guarding
	UpdateStatus("Guarding")

func _on_WindUpButton_pressed():
	CurrentStatus = fighter_status.Winding_Up
	UpdateStatus("Winding Up")

func _on_HeavyButton_pressed():
	CurrentStatus = fighter_status.Recoiling
	UpdateStatus("Heavy")
